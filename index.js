"use strict"

// Завдання

// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та
//  отримати список усіх фільмів серії Зоряні війни
// // Для кожного фільму отримати з сервера список персонажів, 
// які були показані у цьому фільмі.Список персонажів можна отримати з властивості characters.
// // Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів
//  на екрані.Необхідно вказати номер епізоду, назву фільму,
//     а також короткий зміст(поля episodeId, name, openingCrawl).
// // Як тільки з сервера буде отримано інформацію про персонажів будь - якого фільму, 
// вивести цю інформацію на екран під назвою фільму.

const ul = document.querySelector('.ul');

const xhr = new XMLHttpRequest();
const url = 'https://ajax.test-danit.com/api/swapi/films';
xhr.open('GET', url, true);
xhr.send();
xhr.onreadystatechange = () => {
    
    if (xhr.readyState === 4 && xhr.status === 200) {
        const filmsArray = JSON.parse(xhr.response);
        console.log(filmsArray);
    

        filmsArray.forEach(({ episodeId, name, openingCrawl, characters, id }) => {
            ul.insertAdjacentHTML('beforeend', `
            <li id='${id}' > Name: ${name}<br>
                 Episode: ${episodeId}<br>
                 Content:${openingCrawl}
            </li>
           `);
        

            // console.log(characters);

            const filmElement = document.getElementById(`${id}`);
            characters.forEach(charactersURL => {
                const charactersXHR = new XMLHttpRequest();
                charactersXHR.open('GET', charactersURL, true);

                charactersXHR.onload = function () {
                    if (charactersXHR.status === 200) {
                        const {name} = JSON.parse(charactersXHR.responseText);
                        
                        // console.log(name);
                       
                        filmElement.insertAdjacentHTML("beforeend", `
                         <li> ${name}</li>
                        `)
                    }
                    else {
                        console.error('Помилка при отриманні даних');
                    } 
                };
                charactersXHR.send();
            });
        });


    }
};



// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// AJAX(Asynchronous JavaScript and XML) - це технологія, яка дозволяє веб - сторінці взаємодіяти з сервером без перезавантаження всієї сторінки.Завдяки AJAX, можливий асинхронний обмін даними між клієнтом і сервером у фоновому режимі без перерви для користувача.AJAX використовує комбінацію технологій, таких як JavaScript, XML(тепер частіше JSON) і HTTP, для взаємодії з сервером та оновлення частини сторінки.

// Основні переваги використання AJAX у веб - розробці полягають у такому:
// 1. Асинхронність: Запити на сервер відбуваються асинхронно, що означає, що користувач може продовжувати взаємодію зі сторінкою, поки дані завантажуються з сервера.
// 2. Відсутність перезавантаження: AJAX дозволяє оновлювати лише частину сторінки, яка потребує оновлення, замість перезавантаження всієї сторінки, що покращує швидкість та користувацький досвід.
// 3. Динамічність: Завдяки AJAX можлива динамічна підгрузка даних, вмісту та взаємодія з користувачем без необхідності повного оновлення сторінки.